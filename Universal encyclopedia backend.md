# Introduction

This idea came to my mind gradually while I was rewriting an older article of mine about how I would improve Wikipedia or create my own online encyclopedia.

At some point I realized it could be better to have a universal knowledge database rather than another Wikipedia.

# Philosophy of splitting tasks

On computer engineering, a field on which I’m no more than a newbie, there’s a method to improve a program’s maintenance and compatibility by splitting it into simpler programs with simpler tasks and communicate them. You can see this approach applied to PCs, software libraries, work pipes, web services… And obviously there must be a ton of examples outside computers, however those are the ones I know better due to my hobby.

You can take many approaches when decomposing an encyclopedia. Usually there’s a clear division between *content* and *user interface*, also widely referred as as "back-end" and "front-end". I wont waste time explaining what they are on detail since the following can be understood without knowing it. A second split I made on the content was between information plus sources, and narrative explanation plus examples. On that difference between facts management and article redaction is where I saw an opportunity to improve online encyclopedias: Some kind of reusable facts management system.

# Facts database

My idea for this "facts management system" is a simple computable yet human readable database, so you don’t need any complex software to interact with it.

Since it will be using mostly text, I find document oriented database paradigm a good contender. 

# Deciding what’s truth

Here is the big deal. How do we decide what’s worthy of belonging to this omniscient database and therefore to be considered truth? We simply don’t. On this one I will follow Wikipedia’s approach and not try to be a ruler of truth, but a mere information holder.

That means, every fact can have different versions, and (probably) most of them false. So we better provide some sort of source’s classification to allow users filter what they individually find trust worthy.

I don’t have enough epistemological nor gnoseological background to set up this classification system, however I imagine it could be based on how a knowledge was obtained. Which method or methods were used. For example, multimedia proofs like pictures or TV recordings, mathematical deductions, computer simulations, book quotes, social experiments with p>k, based on other facts, etc.

# Anti-vandalism measures

Where there’s an open collaboration project, there’s also some risk of vandalism. Every encyclopedia has its own moderation system. Wikipedia, for example, has a pretty open policy stating everyone is able to modify almost any page with no need for registration or consensus. That of course lets them vulnerable against vandalism, however they managed to suppress it using tools and methods like *edit history*, *changes reversion*, *recent changes patrolling*, *protected articles*, and *IP blocking*.

On the other hand we have *version control systems* (VCS), which were created to manage much more delicate documents: software source code. On their approach, anyone can propose an edit, but its approval must be made by the right administrator.

Looking for a middle field between freedom and security I came up with the following system:

1. **Anyone can propose an edit**: Users don’t need registration of any kind to propose an edition, creation, or deletion of any entry on the database. Just passing a rather hard test to filter spam.
2. **Voting for approval**: For a proposal to be accepted, it must be approved for the community though some sophisticated voting system that I will explain later.
3. **Discussion boards**: Every proposal must have a free speach discussion board for authenticated users and its creator in case it’s anonymous.
4. **Record of changes**: Keep an historical record of changes so no information, true or false, is lost.

# Data structure

Before choosing a data structure to represent all the information we have to store, since there are several that could do the job, I’m going to explain what data fields must be in there and how are they related to each other, that with the intention of clarifying my decision.

In our knowledge model, a *fact* is a **propositional text** that has a **source**, a **title**, and a **classification**. Such classification defines what thing or *concept* a fact is referring to: for example, "what was happening at David Hilbert’s birthday", "the number of stars on Andromeda", or "how to use a chainsaw".

Concepts are key to relate facts among them. If two facts refer to similar concepts, for example "size of the *sun*" and "temperature of the *sun*", they should be easily associated. And to define their relationships I propose a classification system heavily inspired on *Object Oriented Programming* since it uses *objects*, *classes*, and *properties*:

Now a fact refers to a **property** or dimension of a real world **object**, like for example "the sun’s mass", "Italia’s history", "giraffes taxonomic classification"… remember that *actions* and *characteristics* work as objects when nominalized. Those objects could be grouped into **classes** with properties they would inherit. And classes could be grouped too on other classes, just like OOP.

----

![](./Media/scheme universal encyclopedia backend.drawio.png)

----

An example

![](./Media/scheme universal encyclopedia backend example.drawio.png)

# Supported languages

In a world full of languages and dialects, where any person would prefer to read and write on their mother language, some sources of like books, magazines, but specially encyclopedias, have tried to diversify their language repertoire and reach a larger audience. Unfortunately such diversification entails a division of their contributor’s efforts. A prize most platforms are willing to pay. However, what if we had an alternative? I think an interesting solution  to the issue would be using *formalized languages*.

A formalized language is just a language like any other, natural or constructed, but with a formalized grammar. Which means its syntax, semantics, and pragmatics have been prestablished and they are always the same everywhere it is used. That makes them extemely simple to translate into any other language, even automatically through the use of software. Making it possible for collaborators focus on a single instance of information.

Now, there are multiple languages that could perform this task, Esperanto and Interlingua being the most famous, however, they are not even close to the number of speakers of most natural languages, and this is important when it comes to reaching collaborators. There is no choice but to take an already heavily used language and formalize it so it can be automatically translated to any other language.

We already deveoped **formalized versions of English** like *"Simplified Technical English"*, *"Basic English"*, *"Learning English"*, and so on. And there is also an existing [Wikipedia instance](https://en.wikipedia.org/wiki/Simple_English_Wikipedia) based on this languages. However, I’m not gonna bother to choose one of them without consulting with a linguist. Who knows if it ends up being other than English.

# Extra concerns

- Funding and support: TODO
- Voting system: TODO
- Some classes I thought: TODO

