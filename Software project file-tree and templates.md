# Introduction

Usually, software development projects comprise many sorts of files of which developer tools require some guarantees on their presence, placement, and formatting. Those circumstances across several tools end up on a spontaneous file-tree where most configuration file is at the root folder of the project.

In my personal opinion classifying those files contextually could help new contributors to find a place in a project. Specially when combined with a minimum of standardized documentation files. That’s what I will try to provide to my future repositories: an standardized file-tree and documentation templates.

# File-tree

As mentioned before, many tools will expect their files placed somewhere else on the file-tree. Therefore one can only get so far when it comes to personalize all this before things start breaking. 

- ***README.md*** — An introduction to the project. The only file that must be on the root folder of the project
- **src/** — Main source code files
- **test/** — Small programs to test the source code
- **devenv/** — Configuration files for the developement and deployment tools (examples: *.gitignore*, *.devcontainer/*, *.editorconfig*, *Makefile*, *travis.yml*, *Dockerfile*…)
- **media/** — Media files that are used in documentation and other files
- **examples/** — Examples of the product 
- **docs/** — Documentation files
  - ***LICENSE.txt*** — Source code’s legal terms of use
  - ***INSTALL.md*** — Instructions to install this program from source
  - ***FAQ.md*** — Frequetly asked questions
  - ***CONTRIBUTE.md*** — All the information you need to contribute to the project
  - ***ROADMAP.md*** — Roadmap of the project. What we’ve done and what we’ll do next
  - ***NEWS.md*** — Announcements about the project that might interest users
  - ***CHANGELOG.md*** — Detailed chronology of changes that the project has suffered until this date
  - ***code_of_conduct.md*** — Describes our code of conduct, used at every interaction related to this project
  - ***pull_request_template.md*** — Template to fulfill when doing pull requests
  - ***issue_template.md*** — Template to fulfill when reporting a bug or enhancement.

# LICENSE.txt

Code licensing is a trivial but extremely decisive procedure. It must not be taken lightly as it determines how your code can be used, and therefore how the users community will adopt it. Fortunately there are some webpages out there to help new developers choose their license.

# INSTALL.md

```markdown
Index

# Platform
Steps

# Platform
Steps

...
```

# FAQ.md

```markdown
Index

# Question
Answer

# Question
Answer

...
```

# CONTRIBUTE.md

Guide of all the possible ways anyone can contribute to the project. Developer or not.

```markdown
# Introduction
You can contribute to the project through any of the following actions:

1. Help with the [development](#Development)
2. Bug reports, enhancement suggestions, and any other kind of [feedback](#Feedback).
3. Help with the [translation](#Translations)
4. Make a [donation](#Donations)
5. [Share your experience](#Community) with others

Also, we suggest you to follow our [*code of conduct*](code_of_conduct.md) at every interaction with the project.


# Development
- **Design choices**
  - **Platforms**: TODO
  - **Languages**: TODO
  - **Design pattern**: TODO
  - **Coding convention**: TODO
  - **Versioning system**: [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html)
  - **Pull requests protocol**: Follow the [project template](./pull_request_template.md)
  - **Unit testing style**: TODO
  - **UI convention**: TODO
- **Tooling choices** - All configuration files can be found on the [*devenv/*](../devenv/) folder
  - **Compilers/interpreters**: TODO
  - **REPL tool**: TODO
  - **VCS tool**: TODO
  - **Package manager**: TODO
  - **Build automation tool**: TODO
  - **Automatic deployment manager**: TODO
  - **LSP**: TODO
  - **Code formater**: TODO
  - **Containerized coding environment**: TODO
- **Libraries, frameworks and other dependencies**
  - TODO


# Feedback
You can report bugs, request features, and suggest enhancements by fulfilling a formal template stored on the *docs/* folder as *[issue_template.md](./issue_template.md)*.


# Translations
Official translations:
- TODO
- TODO
- ...


# Donations
Supported donation platforms:
- TODO
- TODO
- ...


# Community
Supported communication channels:
- TODO
- TODO
- ...
```

# ROADMAP.md

List of tasks that developers plan to work on, and goals to reach. Nowadays this document isn’t as detailed and updated as other specialized tools (Gitlab, Github, Trello…). Ideally all this toolchain should backup on text files inside the project itself so it serves as a universal access point to the project’s state independently of any tool, platform, connection, etc.

```markdown
Index

# Working on
- TODO
- TODO
- ...

# High priority / Easy to accomplish
- TODO
- TODO
- ...

# Low priority / Hard to accomplish
- TODO
- TODO
- ...

# Done
- TODO
- TODO
- ...
```

# NEWS.md

Highly detailed announcements of project updates (code, administration, goals…) that might interest the final user. Those announcements, like the updates they talk about, should be occasional, big, and always incremental (older ones must not be deleted, but moved a spot down)

```markdown
Index

# TITLE
Date (DD/MM/YYYY) — Announcement

# TITLE
Date (DD/MM/YYYY) — Announcement

...
```

# CHANGELOG.md

Log of every update on the project’s source code, chronologically sorted from top to bottom, and representing its versioning system through markdown’s title hierarchy.

```markdown
Index

# Release name — Date (DD/MM/YYYY)
Date (DD/MM/YYYY) — Announcement

## Minor release name — Date (DD/MM/YYYY)
Date (DD/MM/YYYY) — Announcement

## Minor release name — Date (DD/MM/YYYY)
Date (DD/MM/YYYY) — Announcement

...
```

# code_of_conduct.md

This document describes a code of conduct to follow on any official interaction with other contributors. It’s designed to guarantee a comfortable communication among the community of a project.

An example as good as popular is https://www.contributor-covenant.org/

# pull_request_template.md

```markdown
# Description
Please include a summary of the change and which issue is fixed. Please also include relevant motivation and context. List any dependencies that are required for this change.

## Type of change
Please delete options that are not relevant.
- [ ] Bug fix (non-breaking change which fixes an issue)
- [ ] Enhancement
- [ ] New feature (non-breaking change which adds functionality)
- [ ] Breaking change (fix or feature that would cause existing functionality to not work as expected)
- [ ] This change requires a documentation update

# How Has This Been Tested?
Please describe the tests that you ran to verify your changes. Provide instructions so we can reproduce. Please also list any relevant details for your test configuration

# Checklist:
- [ ] My code follows the style guidelines of this project
- [ ] I have performed a self-review of my own code
- [ ] I have commented my code, particularly in hard-to-understand areas
- [ ] I have made corresponding changes to the documentation
- [ ] My changes generate no new warnings
```

Source: https://embeddedartistry.com/blog/2017/08/04/a-github-pull-request-template-for-your-projects/

# issue_template.md

```markdown
# Prerequisites
Please answer the following questions for yourself before submitting an issue.
- [ ] I am running the latest version
- [ ] I checked the documentation and found no answer
- [ ] I checked to make sure that this issue has not already been filed

# Expected Behavior
Please describe the behavior you are expecting

# Current Behavior
What is the current behavior?

# Failure Information (for bugs)
Please help provide information about the failure if this is a bug. If it is not a bug, please remove the rest of this template.

## Steps to Reproduce
Please provide detailed steps for reproducing the issue.
1. step 1
2. step 2
3. you get it...

## Context
Please provide any relevant information about your setup. This is important in case the issue is not reproducible except for under certain conditions.
* Firmware Version:
* Operating System:
* SDK version:
* Toolchain involved:

## Failure Logs
Please include any relevant log snippets or files here.
```

Source: I don’t remember. It’s an old one.













