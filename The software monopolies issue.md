# Introduction

There is this issue on the software apps ecosystem, that at any level of user knowledge, from inexperienced users to developers and powerusers, there exists a huge excess of incomplete products, in the sense of them not having enough features to satisfy every one of its users.

It forces most people to rely on several solutions to address the same kind of *material* (notes, videochats, pictures, comments…), or even worse, to let many of their necessities uncovered just to avoid the hassle of learning and managing more tools. Some examples related to de previous ones could be writing and managing notes, host and attend videochats, edit pictures, write and review comments…

# Issue origin

Situations like previously described don’t come out intentionally. Developers do they best to satisfy as many users, and cover as many necessities as they can. However it seems like that’s never enough since the demand of features is too large, diverse, and sometimes even contradictory (some features can’t come together).

Of course on any healthy market, if there are necessities uncovered, sooner or later there will be an entrepreneur movement to benefit from it by covering such necessities with a custom-made solution. Why isn’t happening here?

While I lack any proper tool to perform a decent study, I hypothesize in a totally subjective basis that the issue lies on the software market having a *user-product dependency*. Such condition ends up generating feature’s monopolies and prevents custom-made solutions of being profitable.

## User product relationship

For any product, its ratio between makers and users population can fall into one of two specific groups:

- User-product independence: Makers population is much lower than users, and it doesn’t matter if makers are also users. Changes in users population won’t affect on the product unless makers decide it.
- User-product codependency: Makers population is comparable or equal to users, and makers themselves are part of the userbase. Changes in users population directly affect on makers population, hence on the product. To the point where it can unleash a chain effect where product’s quality/quantity and users population increase or decrease retroactively.

Some examples of the first case are classical market shops and most industrial fabrics, while on the other hand, examples of the user-product codependency are nightclubs (clients are also the reclaim to other clients), social networks (users consume but also create media for other users), and software with plugins market (some of their users are also plugins developers and maintainers).

But, why is user-product codependency an issue? When a product quality depends on its community, competence not only has to develop a better product to compete, but also rely on an already existing community to support it. Which turns out on a much lower and harder emergence of competence and a slow transition to monopoly markets (Youtube, Windows, Facebook…)

# Solution

My solution to avoid said monopolies relies on creating a standardization for features (procedures or algorithms in essence) the same way we did with multimedia objects (JPG, MP4, HTML…) and text encoding (ASCII, UTF-8, UTF-16…). This way we can access them universally no matter the computer, framework or programming language we are using.

It looks pretty much like an API or FFI, however contrary to them, this solution would not be "called" or "executed", but rather "interpreted" in a *declarative* way. That means it doesn’t do or tells how to do any task or features, but rather what the results must be, so each language can implement it on its own terms like it’s a configuration file.

I imagine some requirements for the standard to success as a real solution are:

- **Speed**: No speed loss compared to native algorithms.
- **Universality**: Language-agnosticism. No links to any language or framewoek.
- **Composability**: Features can be grouped and stacked to build more complex ones, like bricks on a building.
- **Declarative**: Declarative and referential transparent (a.k.a. pure functional) to improve speed and security while allowing it to be interpreted from any language.
- **Versatility**: Useful to describe any kind of feature. Databases, graphic processors, GUIs, OS…

The final goal is to allow a feature’s ecosystem where any user, no matter its knowledge or programming language, can take advantage of what other users and developers have created and quickly build custom-made solution by themselves with the same ease a senior developer could do it nowadays using software libraries, FFIs, APIs, frameworks…

Such goal isn’t something new on the horizon since it’s been tried to accomplish through several projects I was inspired by, like Emacs, Unix command-line tools, plug-ins stores, some libraries… unfortunately non of them achieving all the requirements to, in my opinion, succeed.

I also don’t know how an approach to this ideal solution should be. Maybe some kind of pure functional bytecode or user friendly IR language… whatever it is, I hope for a future to comeback wiser and make some progress on it.
