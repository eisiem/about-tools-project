# Introduction

For years the only two ways to reward online content creation have been direct donations and commercial advertisements. However, what started as a few small images on websites’ empty spaces, soon became a nightmare made of intrusive ads and abusive personal data trafficking (way more dangerous than it seems) to every internet user who dared to navigate without *adblocker*.

War between content creators and consumers may be at its peak right now on late 2023, when big content platforms have opened a campaign against adbockers as they got popular enough to bother them, the same time some public institutions are starting to punish big data harvest due to new concerns about its risks.

The internet community needs for a pacific solution. Content creators have a right to be rewarded and recognized by their work, the same way consumers have a right to a net free of ads and *spyware*, and full of content. Unfortunately I haven’t seen yet any proposed solution that can grant those four demands: rewards, no ads, no spyware, and many content.

# Micro-tips browser extension

Inspired by Brave’s cryptocurrency wallet and Liberapay’s micro-sponsorship, I have devised what to my opinion could be a solution that fulfills all four demands previously mentioned if implemented correctly.

It would came as a browser extension with which any user could manage its own *cryptowallet* and fill it through the rent of computer resources to mine cryptocurrency. In other words, you would exchange "online money" for your PC’s resources instead of you attention and time (ads), or personal data (big data).

Through the same extension, users would be able to donate occasionally and periodically a share of their savings to any piece of content of their liking, identifying its ownership through URLs the same way Liberapay does.

# Implementation details

- **Anonymous**: Wallet address and personal data must not be linked by any means. And additional options for anonymity must be provided, such as temporal addresses.
- **Universal**: Users and content creators shouldn’t face any obstacles when interacting with the system, such as register credentials or using special compatible technologies besides any modern browser. That alone should boost its adoption growth. 
- **Configurable**: Every option regardless resources mining, donation giving, and privacy must be configurable.
- **Decentralized**: Take advantage of the decentralized nature of cryptocurrency and work independently to any banking system.
- **Free and *Open source***: I think there’s no need to explain why a decentralized platform should be open source, and what other benefits come from it.
- **Donation plans**: Not everyone has the same standards when tipping. A handful of rule’s systems must be provided to satisfy as many users as possible. Some examples are: Occasional tipping on the current URLs or an specific multimedia element, periodically equally tip the most used websites, periodically equally tip the most liked websites…
- **Traceable**: Within a total anonymity concern, users should be able to track donations associated to its wallet address, where they come and where they go. Whether it’s a device specific address automatically generated, an external wallet address used on multiple devices and with many other usecases outside the extension, or even temporary addresses optionally outputting  (though email for example) a log of donations once they expire.
- **Non interactive**: Maybe the most disconcerting feature. It would be easy for web developers to exploit the wallet address as online ID, then demanding pay to access content. That would go against the goal of providing for a web full of content. Also, I consider that managing online ID’s and subscriptions is other product’s job, related or not to cryptocurrency.

# Obstacles

Of course I doubt I’m the first worried mind that comes up with a similar idea, although I don’t see any implementations saving the internet yet. Therefore there must be some obstacles preventing it from becoming a viable solution. Two of which I suppose are the following:

- **Identifying the original content creator**: When content is on a standalone website, or even on a profile inside a social media platform, it’s more or less easy to prove its authorship. Trouble comes when that content is just shared. How do you trace it to its source? Worse, what if it’s re-uploaded? The only solution I can think of is creating some sort of HTML metadata tag to credit original authors.
- **Reaching a meaningful value for the coin**: Even though you can actually create value out of renting computer resources, comparable to that of your attention, time, and personal data that right now we use as trade for "free" content; such resource consumption is (necessarily) minimal next to what a dedicated PC could take. That means what an average target user could earn wouldn’t be of much value, yet it would reduce notably it device’s battery. In other words, basically any 5yo desktop PC can process in 30min what a brand new smartphone using the extension could process on a whole month of internet browsing in exchange for 5-10% of its battery life.
