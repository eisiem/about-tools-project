# Introducción

Lo que estoy a punto de escribir son las conclusiones a las que he llegado después de unos cuantos paseos por libros, vídeos, y otros tipos de contenido multimedia, didácticos. Y lo hago no con el objetivo de convencer a cualquier lector que se tope con este ensayo, sino de dejar constancia de mi ideología para observar en un futuro la evolución de mi pensamiento, seguramente avergonzado de mi ignorancia actual.

Cabe recalcar que mis conocimientos en cualquier materia de estudio son meramente superficiales, fruto de la curiosidad y la procastinación. Para evitar que esto siga así en la siguiente revisión, al final del ensayo listaré una serie de temas, lo más concretos posibles, sobre los que me gustaría informarme en profundidad antes de considerarme más sabio.

A partir de aquí todo lo que hay es opinión, al que no me referiré como tal para aligerar la lectura. En otras palabras, no verás muchos «creo que», «pienso que», o «considero que», la subjetividad se encuentra implícita a lo largo de todo el texto.

# Dinámica de la lectura

No me considero un escritor decente. Paso horas y horas reescribiendo párrafos con tal de ser más claro, sin llegar a un punto satisfactorio, y perdiendo carisma por el camino.
Es por esta razón que voy a utilizar dos estilos mediocres como ellos solos para redactar este texto. Por un lado habrá párrafos puramente coloquiales como este que estoy escribiendo, en los que teclearé las palabras conforme vengan a mi cabeza, con alguna que otra corrección. Y en contraposición, con un estilo más formalizado para las secciones donde describo mi filosofía, trabajaré en plasmar de forma clara y concisa la información con a siguiente estructura: párrafo monotemático expositivo, resumen conciso, separador. Quizás añadiendo una introducción a principio de sección.