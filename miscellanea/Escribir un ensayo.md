# Introducción

A través de este ensayo busco recopilar y reflaxionar todos los pequeños trucos y pautas que sigo a la hora de escribir ensayos semejantes, valga la redundancia.

# Qué es un ensayo

Aunque el lector medio asocia el término *ensayo* a un artículo técnico, extremadamente complejo, y probablmente profesional, realmente un no es más que un texto a través del cual el autor busca reflexionar él mismo y hacer reflexionar a sus lectores acerca de un tema en concreto. Ese tema puede entonces ser más riguroso, como aquellos filosóficos, políticos o científicos, o algo más coloquiales y populares, como redacciones subjetivas de noticias o *blogposts*.

# Estructura de un ensayo

Cada tipo de ensayo cuenta con unas directrices más o menos consensuadas para ser considerado por su comunidad como parte de ella. Sin embargo mi objetivo dista mucho de generar otra de estas guías, y se acerca más a una reflexión sobre el proceso de escritura independientemente de su finalidad o comunidad.

Como escritores, nuestro objetivo es transformar un revoltijo de información en nuestra mente en una idea bien estructurada y coherente que no solo otros puedan entender, sino que también nos permita ordenar y vislumbrar fallos en nuestras ideas. Es decir, que a la hora de empezar a escribir, el propio autor no sabe cómo va a concluir su obra, por lo que un buen planteamiento inicial es clave a la hora de acelerar el refinamiento de las ideas del autor.

El método que utilizo es un clásico de las redacciones: 

- Empezamos con una **lluvia de ideas** en lo alto del todo, a modo de *checkbox* o *TODO list*, la cual iremos actualizando a medida que avancemos en la escritura, y eliminaremos al final de esta.
- Después seguimos con una **introducción** en la que dejamos claro la temática del ensayo, el objetivo que se pretende alcanzar, los temas a reflexionar, y el orden en el que se pretenden abordar.
- Muy seguido podemos dedicar una sección a crear un **glosario** de términos con los que el lector a lo mejor no es del todo familiar, necesita recordar, o simplemente son ambigüos y buscamos aclarar cual de sus acepciones estaremos usando. Estos términos pertenecen al pretexto de la idea y su consulta en el glosario habrá de ser auxiliar. Si hubiera algún concepto nuevo para el lector, este habrá de explicarse en el cuerpo de la obra. Por otra parte, el glosario suele ampliarse y modificarse a medida que el ensayo se desarrolla.
- Ahora viene el **cuerpo** del artículo, que es el núcleo donde habita la idea a reflexionar. En la siguiente sección hablaré en detalle de cómo desglosar y ordenar esta idea.
- Por último cerramos con una sección de **conclusión**, donde podemos ser mucho más subjetivos y manifestar nuestro opinión, esperanzas, nuevas ideas, nuevas dudas, etc.

# Descomposición de una idea

A día de hoy todavía no hemos hallado una forma inequívoca de averigüar de la estructura de un pensamiento o idea, sin embargo podemos aproximarnos a esta si nos fijamos en su materialización: el lenguaje. Asumiendo que en efecto el lenguaje es un reflejo más o menos fideligno del pensamiento.

En su forma más simple la oración ha de contener mínimo un sujeto y un predicado (omitidos o no) para albergar sentido, es decir, para llevar una idea a nuestra mente. Y todo parece apuntar a que el ser humano no es capaz de concebir una idea más simple que *algo* (sujeto) haciendo *algo* (predicado). Aunque por supuesto esto es tema de debate primero filosófico y luego científico.

Partiendo de eso, podemos asumir que cualquier idea elaborada puede ser descompuesta en ideas más simples hasta llegar a esta especie de unidad mínima de concepción con la forma "cosa realizando acción". Y el conjunto de varias de estas unidades, cuando interrelacionadas y comunicadas mediante recursos lingüísticos y ortotipográficos, es lo que compone los textos.

Podría ser de otra manera, pero no sería algo sencillo de leer. Podríamos juntar ideas debilmente interrelacionadas, y el ensayo se asemejaría más a una lluvia de ideas que a una obra planificada. O podríamos prescindir de las figuras literarias y ortotipográficas que las comunican, pero la reiterada alusión a las "cosas" y "acciones" involucradas en las ideas expresadas acabarían con la paciencia de cualquier lector.

# Figuras literarias

Retomando un poco el último párrafo de la anterior sección, si eres un lector más o menos experienciado ya te habrás percatado de los desperfectos narrativos presentes en al menos esta obra. Y es que la falta de talento redactor así como mi obsesión por la concisión (que me hace huir de figuras literarias decorativas), convergen en párrafos cargados de redudancias sintácticas como alusiones reiteradas a un concepto, o numerosos *conectores causales* ("ya que", "por lo tanto", "entonces"…) introduciendo cada paso de mis argumentos.

Los recursos lingüísticos con capacidad de aligerar la lectura rondan las decenas en número y gozan de una diversidad de igual magnitud: elipsis, metáfora, sinécdoque, personificación… La verdadera dificultad aparece a la hora de implementarlas, pues no existe ninguna regla o criterio claro para hacerlo sistemáticamente. De manera que tristemente mi estrategia para acabar con el sobrepeso sintáctico consiste revisar lo escrito constantemente en busca de redundancias e intentar sustituirlas por algún sinónimo o figura, buscando siempre la mayor claridad posible.

# Markdown

Mucho más numerosos que los recursos lingüísticos son los ortoripográficos, que se cuentan por los cientos. Al igual que sus primos se utilizan para aligerar la carga sintáctica de un texto, pero a diferencia de los otros los recursos lingüísticos no interfieren con la elección de palabras, sino con su presentación: párrafos, legra negrita, titulares, tablas, listas, letra subraya, flechas, esquemas, cambios de tipografía, recuadros, etc.

En el mercado de software abundan las herramientas que permiten escribir texto enriquecido con sus correspondientes conjuntos de recursos ortotipográficos a disposición del usuario. Yo personalmente he utilizado demasiados, lo que me ha permitido aislar aquellas características que considero indispensables a la hora de escribir, y encontrar la herramienta más acorde a estas: *Markdown*, un lenguaje de marcas que no es perfecto, pero sí sobresaliente.

Lo primero que hay que saber sobre Markdown es que es muy simple: provee una serie de recursos tipográficos limitados que pueden resultar insuficientes para algunos escritores con ansias de personalizar hasta los saltos de línea, pero que al dejarse llevar y acomodar las ideas de uno a la estructura que propone el lenguaje, encontrará más que satisfactorio un resultado más conciso y ordenado que su idea original.

Otra de sus grandes ventajas es sin duda que se trata de *código fuente*. Esto quiere decir que el contenido y la presentación de una obra existen y pueden ser editados por separado. De esta manera el escritor tan solo ha de concentrarse en el texto y delagar al futuro la apariencia final de su obra. Aquí Markdown sobresale respecto a la media con una colección de etiquetas tan escueta que puede distinguirse fácilmente incluso antes de ser procesado y dotado de una apariencia presentable.

Por último cuenta con una característica muy relevante que normalmente es pasada por alto al coincidir con la gran mayoría de las otras herramientas: su conjunto de recursos ortotipográficos nos obliga a estructurar el texto y por lo tanto nuestra idea de forma *jerárquica*. Esto quiere decir que las ideas escritas dentro de una etiqueta comparten significado con las etiquetas "superiores". Por ejemplo si listamos una serie de minerales (pirita, cuarzo, esmeralda…) bajo el título "piedras que me gustan", todas aquellas piedras listadas compartirán significado con el título y podrá determinarse que me gustan.

En resumen Markdown me facilita la composición de ensayos y me despoja de cualquier queja que pueda tener en este ámbito. Aunque ya tengo echado el ojo a alternativas más complejas ([Typst](https://www.typst.app)) por si agún día necesito algo más de personalización.

